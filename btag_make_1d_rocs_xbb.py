#!/usr/bin/env python3

"""
build some roc curves
"""

from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
from collections import defaultdict

def get_args():
    epilog='Get back to work'
    parser = ArgumentParser(description=__doc__, epilog=epilog)
    parser.add_argument('datasets', nargs='+', type=Path)
    parser.add_argument('-o', '--out-file', default='roc1d.h5',
                        type=Path, help='output hist file')
    parser.add_argument('-m', '--dr-matching', default=0.2, type=float,
                        help='default %(default)s')
    return parser.parse_args()

def is_offline_matched(name):
    return name.startswith('OfflineMatched')

#==================================================================
# main()
#==================================================================
def run():

    #Get args
    args = get_args()
    #================
    # Prepare Output directory
    #================
    #Get out-dir
    outfile = args.out_file
    outfile.parent.mkdir(parents=True, exist_ok=True)
    outfile.unlink(missing_ok=True)

    infar = np.array([np.inf])
    edges = np.concatenate([-infar,np.linspace(-20,20,1000),infar])
    taggers = [
            'Xbb2020v3'
    ]
    dr = args.dr_matching

    hists = defaultdict(lambda: defaultdict(int))
    for input_file in args.datasets:
        with File(input_file, 'r') as h5file:
            jets = h5file['jets']
            for tagger in taggers:
                for label in ["Higgs", "Top", "QCD"]:
                    print(f'building {tagger}, for {label}')
                    hists[tagger][label] += get_hist(
                        jets, edges, label, tagger, dr_match=dr)

    with File(outfile, 'w') as outh5:
        for tagger, lab_dict in hists.items():
            lab_group = outh5.require_group(tagger)
            lab_group.attrs['limits'] = [edges[1], edges[-2]]
            if is_offline_matched(tagger):
                lab_group.attrs['dR_match'] = dr
            for label, hist in lab_dict.items():
                lab_group.create_dataset(label, data=hist)

def get_hist(jets, edges, label, tagger, dr_match):
    if is_offline_matched(tagger):
        valid = jets['OfflineMatchedHadronConeExclTruthLabelID'] == label
        valid &= jets['deltaRToOfflineJet'] < dr_match
    else:
        if label == "Higgs":
            valid = jets['HadronConeExclExtendedTruthLabelID'] == 55
            valid &= jets['n_truth_higgs'] == 1
        elif label == "Top":
            valid = jets['n_truth_top'] == 1
        elif label == "QCD":
            valid = jets['n_truth_higgs'] == 0
            valid &= jets['n_truth_top'] == 0
    truth_tagged = jets[valid]
    flav = {f:truth_tagged[f'{tagger}_{f}'] for f in ["Higgs", "Top", "QCD"]}
    ftop = 0.1
    discrim = np.log(flav['Higgs'] / (ftop * flav['Top'] + (1-ftop) * flav['QCD']))

    return np.histogram(discrim, edges)[0]

if __name__ == '__main__':
    run()
