#!/usr/bin/env python3

"""
Find working points
"""


from argparse import ArgumentParser
from h5py import File
import numpy as np
from pathlib import Path
from sys import stdout, stdin
import json

from math import inf, nan
from collections.abc import Sequence

NULLPATH=Path('-')

def get_args():
    parser = ArgumentParser(description=__doc__)
    parser.add_argument('discrim_hists')
    parser.add_argument('-p','--output-cut-at-percent-working-point',
                        action='store_true')
    mode = parser.add_mutually_exclusive_group()
    mode.add_argument('-e', '--efficiencies', type=float, nargs='+',
                      default=[40, 50, 60, 70, 75, 77, 80, 85, 90, 95])
    mode.add_argument('-w', '--working-points', type=float, nargs='+',
                      default=[])
    mode.add_argument('-j', '--efficiencies-from-json', type=Path, nargs='?',
                      const=NULLPATH)
    return parser.parse_args()


def run():
    args = get_args()

    hists = {}
    annotations = {}

    with File(args.discrim_hists,'r') as roc_file:
        for tagger, flav_group in roc_file.items():
            flav_hists = hists.setdefault(tagger,{})
            annotations[tagger] = dict(flav_group.attrs.items())
            for flav, hist in flav_group.items():
                flav_hists[flav] = np.asarray(hist)

    if args.working_points:
        vals = np.asarray(args.working_points)
        wps = get_working_points(hists, annotations, vals, 'eff')
    elif args.efficiencies_from_json:
        wps = get_eff_from_json(args.efficiencies_from_json, hists,
                                annotations)
    else:
        vals = np.asarray(args.efficiencies) / 100
        wps = get_working_points(hists, annotations, vals, 'wp')

    if args.output_cut_at_percent_working_point:
        out = {}
        for t, twps in wps.items():
            out[t] = {f"{w['eff']*100:.0f}": w['wp'] for w in twps}
    else:
        out = wps

    stdout.write(json.dumps(out, indent=2) + '\n')

def get_eff_from_json(path, hists, annotations):
    if path == NULLPATH:
        if stdin.isatty():
            raise FileNotFoundError("No pipe found")
        wp_dict = json.load(stdin)
    else:
        with open(path) as json_file:
            wp_dict = json.load(json_file)
    out_dict = {}
    for tagger, points in wp_dict.items():
        if not isinstance(points, Sequence):
            points = [{'wp': c, 'eff': int(k)/100} for k, c in points.items()]
        old_points = [{'wp': p['wp'], 'old_eff': p['eff']} for p in points]
        if tagger in hists:
            subhists = {tagger: hists[tagger]}
            vals = [p['wp'] for p in old_points]
            new_points = get_working_points(subhists, annotations, vals, 'eff')
            comb = [o | n for o, n in zip(old_points, new_points[tagger])]
        else:
            comb = old_points
        out_dict[tagger] = comb
    return out_dict

def get_working_points(flav_hists, annotations, vals, output):
    out = {}
    for tagger, flavs in flav_hists.items():
        # the last bin in the downward cumulative sum should be the
        # total, but also corresponds to a cut at negative infinity.
        # We drop it so that we have one efficiency at each bin edge.
        beff = (flavs['b'][::-1].cumsum() / flavs['b'].sum())[:-1]
        low, high = annotations[tagger]['limits']
        edges = np.linspace(high, low, beff.shape[0])
        if output == 'wp':
            cuts = np.interp(vals, beff, edges, left=inf, right=-inf)
            effs = vals
        elif output == 'eff':
            effs = np.interp(vals, edges[::-1], beff[::-1],
                             left=nan, right=nan)
            cuts = vals
        else:
            raise ValueError(f"{output} isn't a known output type")

        out[tagger] = [{'wp':c, 'eff':e} for c, e, in zip(cuts, effs)]

    return out

if __name__ == "__main__":
    run()
